import { Component, OnInit } from '@angular/core';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { ActivatedRoute } from '@angular/router';


class DestinosApiClientViejo {
    getById(id: string): DestinoViaje {
      console.log('llamando por la clase vieja!');
      return null;
    }
  }


@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [  DestinosApiClient,
  // { provide: DestinosApiClientViejo, useExisting: DestinosApiClient }
  ]
})
export class DestinoDetalleComponent implements OnInit {
  destino: DestinoViaje;

  constructor(private route: ActivatedRoute, private destinosApiClient: DestinosApiClient) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    // this.destino = null; // this.destinosApiClient.getById(id);
    this.destino = this.destinosApiClient.getById(id);
  }

}
